﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metier
{
    class Mission
    {
        private string _nom;
        private string _description;
        private int _nbHeuresPrevues;
        private Dictionary<DateTime, int> _relevéHoraire;
        private Intervenant _unIntervenant;

        public string Nom { get => _nom; set => _nom = value; }
        public string Description { get => _description; set => _description = value; }
        public int NbHeuresPrevues { get => _nbHeuresPrevues; set => _nbHeuresPrevues = value; }
        public Dictionary<DateTime, int> RelevéHoraire { get => _relevéHoraire; set => _relevéHoraire = value; }
        public Intervenant UnIntervenant { get => _unIntervenant; set => _unIntervenant = value; }
    }
}
