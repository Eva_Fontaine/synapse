﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metier
{
    class Projet
    {
        private string _nom;
        private DateTime _debut;
        private DateTime _fin;
        private decimal _prixFactureMO;
        private List<Mission> _lesMissions;

        public string Nom { get => _nom; set => _nom = value; }
        public DateTime Debut { get => _debut; set => _debut = value; }
        public DateTime Fin { get => _fin; set => _fin = value; }
        public decimal PrixFactureMO { get => _prixFactureMO; set => _prixFactureMO = value; }
        public List<Mission> LesMissions { get => _lesMissions; set => _lesMissions = value; }

        public decimal MargeBruteCourante()
        {
            decimal MargeBruteCourante = 0;
            decimal calcul = 0;

            foreach (Mission uneMission in _lesMissions)
            {
                calcul = Intervenant.getTauxHoraire() * Mission.getReleveHoraire();
                MargeBruteCourante = PrixFactureMO - calcul;
            }
            return MargeBruteCourante;
        }

        private decimal CumulCoutMO()
        {
            decimal calcul = 0;

            foreach (Mission uneMission in _lesMissions)
            {
                calcul = Intervenant.getTauxHoraire() + Mission.getReleveHoraire();
            }
            return calcul;
        }


    }
}
