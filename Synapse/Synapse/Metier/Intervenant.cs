﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metier
{
    class Intervenant
    {
        private string _nom;
        private decimal _tauxHoraire;

        public string Nom { get => _nom; set => _nom = value; }
        public decimal TauxHoraire { get => _tauxHoraire; set => _tauxHoraire = value; }
        
    }
}
