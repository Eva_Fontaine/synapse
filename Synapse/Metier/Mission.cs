﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Synapse.Metier
{
    class Mission
    {
        private string _nom;
        private string _description;
        private int _nbHeuresPrevues;
        private Dictionary<DateTime,int>_releveHoraire;

        public string Nom { get => _nom; set => _nom = value; }
        public string Description { get => _description; set => _description = value; }
        public int NbHeuresPrevues { get => _nbHeuresPrevues; set => _nbHeuresPrevues = value; }
        public Dictionary<DateTime, int> ReleveHoraire { get => _releveHoraire; set => _releveHoraire = value; }
    }
}
