﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Synapse.Metier
{
    class Intervenant
    {
        private string _nom;
        private decimal _tauxHoraire;

        public string Nom { get => _nom; set => _nom = value; }
        public decimal TauxHoraire { get => _tauxHoraire; set => _tauxHoraire = value; }
    }
}
